###### [https://gitlab.com/FadeevAlexey/jse-06](https://gitlab.com/FadeevAlexey/jse-06)
# Task Manager 1.0.6

A simple console task manager, can help you organize your tasks.

### Built with
  - Java 8
  - Maven 4.0

### Developer
Alexey Fadeev
[alexey.v.fadeev@gmail.com](mailto:alexey.v.fadeev@gmail.com?subject=TaskManager)

### Building from source

```sh
$ git clone http://gitlab.volnenko.school/FadeevAlexey/jse-06.git
$ cd jse-06
$ mvn clean
$ mvn install
```

### Running the application

```sh
$ java -jar target/task-manager-1.0.6.jar
```