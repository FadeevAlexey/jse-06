package ru.fadeev.tm.repository;

import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.EntityDuplicateException;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> tasks = new LinkedHashMap<>();

    public List<Task> findAll() {
        return new LinkedList<>(tasks.values());
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public Task remove(String id) {
        return tasks.remove(id);
    }

    public Task persist(Task task) {
        if (tasks.containsKey(task.getId()))
            throw new EntityDuplicateException();
        return tasks.put(task.getId(), task);
    }

    public Task merge(Task task) {
        return tasks.put(task.getId(), task);
    }

    public void removeAll() {
        tasks.clear();
    }

}