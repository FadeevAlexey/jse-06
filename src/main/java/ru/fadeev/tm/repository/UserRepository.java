package ru.fadeev.tm.repository;

import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.EntityDuplicateException;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private Map<String, User> users = new LinkedHashMap<>();

    public List<User> findAll() {
        return new LinkedList<>(users.values());
    }

    public User findOne(String id) {
        return users.get(id);
    }

    public User remove(String id) {
      return users.remove(id);
    }

    public User persist(User user) {
        if (users.containsKey(user.getId()))
            throw new EntityDuplicateException();
        return users.put(user.getId(), user);
    }

    public User merge(User user) {
        return users.put(user.getId(), user);
    }

    public void removeAll() {
        users.clear();
    }

}