package ru.fadeev.tm.command;

import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public boolean isPermission(User user){
        return user != null;
    }

    public abstract void execute();

    public abstract String getName();

    public abstract String getDescription();

}