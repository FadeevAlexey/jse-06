package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.service.UserService;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isPermission(User user) {
        return true;
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "log in task manager";
    }

    @Override
    public void execute() {
        UserService userService = bootstrap.getUserService();
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN");
        String userId = userService.findIdByName(ConsoleHelper.readString());
        if (userId == null)
            throw new IllegalArgumentException("Can't find user");
        User user = userService.findOne(userId);
        System.out.println("ENTER PASSWORD");
        String userPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!userPassword.equals(user.getHashPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        bootstrap.setCurrentUser(user);
        System.out.println("[OK]\n");
    }

}