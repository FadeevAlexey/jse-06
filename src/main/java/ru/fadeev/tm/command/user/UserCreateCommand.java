package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.service.UserService;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public class UserCreateCommand extends AbstractCommand {

    public UserCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isPermission(User user) {
        return true;
    }

    @Override
    public String getName() {
        return "user-create";
    }

    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() {
        User currentUser = bootstrap.getCurrentUser();
        boolean isAdministrator = currentUser != null && Role.ADMINISTRATOR == currentUser.getRole();
        UserService userService = bootstrap.getUserService();
        System.out.println("[CREATE ACCOUNT]");
        System.out.println("ENTER NAME");
        String name = ConsoleHelper.readString();
        if(name == null || name.isEmpty())
            throw new IllegalUserNameException("Incorrect name");
        String userId = userService.findIdByName(name);
        if (userId != null)
            throw new IllegalUserNameException("User with same name already exist");
        User user = new User(name);
        System.out.println("Enter password:");
        user.setHashPassword(HashUtil.stringToMd5Hash(ConsoleHelper.readString()));
        if (isAdministrator)
            setRole(user);
        userService.persist(user);
        System.out.println("[OK]\n");
    }

    public void setRole(User user) {
        System.out.println("would you like give new account administrator rights? y/n");
        if (ConsoleHelper.readString().equals("y"))
            user.setRole(Role.ADMINISTRATOR);
    }

}