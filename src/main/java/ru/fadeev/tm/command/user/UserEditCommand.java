package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserNameException;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.service.UserService;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public class UserEditCommand extends AbstractCommand {

    public UserEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() {
        UserService userService = bootstrap.getUserService();
        System.out.println("[EDIT PROFILE]");
        System.out.println("ENTER PASSWORD");
        String password = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!password.equals(bootstrap.getCurrentUser().getHashPassword()))
            throw new IllegalUserPasswordException("wrong password, access denied");
        System.out.println("ENTER NEW NAME OR PRESS ENTER");
        String name = ConsoleHelper.readString();
        if(userService.findIdByName(name) != null)
            throw new IllegalUserNameException("User with same name already exist");
        System.out.println("ENTER NEW PASSWORD OR PRESS ENTER");
        String newPassword = ConsoleHelper.readString();
        User user = userService.findOne(bootstrap.getCurrentUser().getId());
        if (newPassword != null && !newPassword.isEmpty())
            user.setHashPassword(HashUtil.stringToMd5Hash(newPassword));
        if (name != null && !name.isEmpty())
            user.setName(name);
        userService.merge(user);
        System.out.println("[OK]\n");
    }

}