package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.exception.IllegalUserPasswordException;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

public class UserUpdatePasswordCommand extends AbstractCommand {

    public UserUpdatePasswordCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() {
        User currentUser = bootstrap.getCurrentUser();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("Enter your current password");
        String currentPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        if (!currentUser.getHashPassword().equals(currentPassword))
            throw new IllegalUserPasswordException("wrong password");
        System.out.println("Enter new password");
        String newPassword = HashUtil.stringToMd5Hash(ConsoleHelper.readString());
        currentUser.setHashPassword(newPassword);
        bootstrap.getUserService().merge(currentUser);
        System.out.println("[OK]\n");
    }

}