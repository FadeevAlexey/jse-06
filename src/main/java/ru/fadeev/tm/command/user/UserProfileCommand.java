package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;

public class UserProfileCommand extends AbstractCommand {

    public UserProfileCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-profile";
    }

    @Override
    public String getDescription() {
        return "Show current profile.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        System.out.println(bootstrap.getCurrentUser() + "\n");
        System.out.println("IF YOU'D LIKE UPDATE PROFILE USE COMMAND: user-edit\n");
    }

}
