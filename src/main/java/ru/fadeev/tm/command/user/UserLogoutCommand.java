package ru.fadeev.tm.command.user;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;

public class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "logout from task-manager";
    }

    @Override
    public void execute() {
        bootstrap.setCurrentUser(null);
        System.out.println("[OK]\n");
    }

}