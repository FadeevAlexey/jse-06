package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Project;

import java.util.List;
import java.util.stream.Collectors;

public final class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        List<Project> projectList = bootstrap.getProjectService().findAll()
                .stream()
                .filter(project -> project.getUserId().equals(bootstrap.getCurrentUser().getId()))
                .collect(Collectors.toList());
        int index = 1;
        for (Project project : projectList) {
            System.out.println(index++ + ". " + project);
        }
        System.out.println();
    }

}