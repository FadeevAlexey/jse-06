package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[EDIT PROJECT]");
        System.out.println("ENTER CURRENT NAME:");
        String name = ConsoleHelper.readString();
        String projectId = projectService.findIdByName(name, bootstrap.getCurrentUser().getId());
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project with name" + name);
        Project project = projectService.findOne(projectId);
        fillFields(project);
        projectService.merge(project);
        System.out.println("[OK]\n");
    }

    private void fillFields(Project project) {
        System.out.println("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        String description = ConsoleHelper.readString();
        System.out.println("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        Date startDate = ConsoleHelper.readDate();
        System.out.println("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        Date finishDate = ConsoleHelper.readDate();
        if (description != null && !description.isEmpty()) project.setDescription(description);
        if (startDate != null) project.setStartDate(startDate);
        if (finishDate != null) project.setFinishDate(finishDate);
    }

}