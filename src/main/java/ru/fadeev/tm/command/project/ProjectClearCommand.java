package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.service.TaskService;

public final class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isPermission(User user) {
      return user != null && Role.ADMINISTRATOR == user.getRole();
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().removeAll();
        TaskService taskService = bootstrap.getTaskService();
        taskService.findAll()
                .forEach(task -> {
                    if (!task.getProjectId().isEmpty())
                        taskService.remove(task.getId());
                });
        System.out.println("[ALL PROJECTS REMOVE]\n");
    }

}