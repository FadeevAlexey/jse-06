package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class ProjectTasksCommand extends AbstractCommand {

    public ProjectTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-tasks";
    }

    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[PROJECT TASKS]");
        System.out.println("ENTER PROJECT NAME");
        String projectId = projectService.findIdByName(ConsoleHelper.readString(),bootstrap.getCurrentUser().getId());
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        int i = 1;
        for (Task task : taskService.findAll()) {
            if (task.getProjectId().equals(projectId))
                System.out.println(i++ + ". " + task);
        }
        System.out.println();
    }

}