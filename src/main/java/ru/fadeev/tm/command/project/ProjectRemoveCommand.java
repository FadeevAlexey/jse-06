package ru.fadeev.tm.command.project;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getProjectService();
        System.out.println("[PROJECT REMOVE]\nENTER NAME");
        String projectId = projectService.findIdByName(ConsoleHelper.readString(),bootstrap.getCurrentUser().getId());
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        projectService.remove(projectId);
        removeAllTasks(projectId);
        System.out.println("[OK]\n");
    }

    private void removeAllTasks(String projectId) {
        TaskService taskService = bootstrap.getTaskService();
        taskService.findAll()
                .stream()
                .filter(task -> task.getProjectId().equals(projectId))
                .forEach(task -> taskService.remove(task.getId()));
    }

}