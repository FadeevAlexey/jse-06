package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public boolean isPermission(User user) {
        return user != null && Role.ADMINISTRATOR == user.getRole();
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        bootstrap.getTaskService().removeAll();
        System.out.println("[ALL TASKS REMOVE]\n");
    }

}