package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.util.ConsoleHelper;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected tasks.";
    }

    @Override
    public void execute() {
        TaskService taskService = bootstrap.getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER NAME");
        String id = taskService.findIdByName(ConsoleHelper.readString(),bootstrap.getCurrentUser().getId());
        if (id == null)
            throw new IllegalProjectNameException("Can't find task");
        taskService.remove(id);
        System.out.println("[OK]\n");
    }

}