package ru.fadeev.tm.command.task;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.context.Bootstrap;
import ru.fadeev.tm.entity.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        List<Task> taskList = bootstrap.getTaskService().findAll()
                .stream()
                .filter(task -> task.getUserId().equals(bootstrap.getCurrentUser().getId()))
                .collect(Collectors.toList());
        int index = 1;
        for (Task task: taskList) {
            System.out.println(index++ + ". " + task);
        }
        System.out.println();
    }

}