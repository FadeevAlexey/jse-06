package ru.fadeev.tm.service;

import ru.fadeev.tm.repository.ProjectRepository;
import ru.fadeev.tm.entity.Project;

import java.util.*;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> findAll() {
        return new LinkedList<>(projectRepository.findAll());
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public Project remove(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.remove(id);
    }

    public Project findOne(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOne(id);
    }

    public Project merge(Project project) {
        if (project == null) return null;
        return projectRepository.merge(project);
    }

    public Project persist(Project project) {
        if (project == null) return null;
        return projectRepository.persist(project);
    }

    public String findIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        Optional<Project> optionalProject = projectRepository
                .findAll()
                .stream()
                .filter(project -> project.getName().equals(name))
                .findAny();
        return optionalProject.map(Project::getId).orElse(null);
    }

    public String findIdByName(String name, String userId) {
        if (name == null || name.isEmpty()) return null;
        Optional<Project> optionalProject = projectRepository
                .findAll()
                .stream()
                .filter(project ->
                        project.getName().equals(name) && project.getUserId().equals(userId))
                .findAny();
        return optionalProject.map(Project::getId).orElse(null);
    }

}