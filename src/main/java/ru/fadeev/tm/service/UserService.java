package ru.fadeev.tm.service;

import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.repository.UserRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository taskRepository) {
        this.userRepository = taskRepository;
    }

    public User persist(User user) {
       if (user == null) return null;
       return userRepository.persist(user);
    }

    public List<User> findAll() {
        return new LinkedList<>(userRepository.findAll());
    }

    public void removeAll() {
        userRepository.removeAll();
    }

    public User remove(String id) {
      if (id == null || id.isEmpty()) return null;
       return userRepository.remove(id);
    }

    public User merge(User user) {
        if (user == null) return null;
      return userRepository.merge(user);
    }

    public User findOne(String id){
        if (id == null || id.isEmpty()) return null;
        return userRepository.findOne(id);
    }

    public String findIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        Optional<User> user = userRepository.findAll()
                .stream()
                .filter(usr -> usr.getName().equals(name))
                .findAny();
        return user.map(User::getId).orElse(null);
    }

}