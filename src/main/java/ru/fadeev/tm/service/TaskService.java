package ru.fadeev.tm.service;

import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task persist(Task task) {
       if (task == null) return null;
       return taskRepository.persist(task);
    }

    public List<Task> findAll() {
        return new LinkedList<>(taskRepository.findAll());
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public Task remove(String id) {
      if (id == null || id.isEmpty()) return null;
       return taskRepository.remove(id);
    }

    public Task merge(Task task) {
        if (task == null) return null;
      return taskRepository.merge(task);
    }

    public Task findOne(String id){
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(id);
    }

    public String findIdByName(String name) {
        if (name == null || name.isEmpty()) return null;
        Optional<Task> task = taskRepository.findAll()
                .stream()
                .filter(tsk -> tsk.getName().equals(name))
                .findAny();
        return task.map(Task::getId).orElse(null);
    }

    public String findIdByName(String name, String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        Optional<Task> task = taskRepository.findAll()
                .stream()
                .filter(tsk -> tsk.getName().equals(name) && tsk.getUserId().equals(userId))
                .findAny();
        return task.map(Task::getId).orElse(null);
    }

}