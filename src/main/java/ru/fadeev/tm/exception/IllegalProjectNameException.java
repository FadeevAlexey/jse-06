package ru.fadeev.tm.exception;

public class IllegalProjectNameException extends RuntimeException {

    public IllegalProjectNameException(String message) {
        super(message);
    }

}
