package ru.fadeev.tm.exception;

public class IllegalTaskNameException extends RuntimeException {

    public IllegalTaskNameException(String message) {
        super(message);
    }

}
