package ru.fadeev.tm.exception;

public class IllegalCommandNameException extends RuntimeException {

    public IllegalCommandNameException(String message) {
        super(message);
    }

}
