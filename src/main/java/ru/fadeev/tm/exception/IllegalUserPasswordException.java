package ru.fadeev.tm.exception;

public class IllegalUserPasswordException extends RuntimeException {

    public IllegalUserPasswordException(String message) {
        super(message);
    }

}
