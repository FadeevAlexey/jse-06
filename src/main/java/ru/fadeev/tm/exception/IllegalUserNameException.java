package ru.fadeev.tm.exception;

public class IllegalUserNameException extends RuntimeException {

    public IllegalUserNameException(String message) {
        super(message);
    }

}
