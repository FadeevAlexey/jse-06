package ru.fadeev.tm.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil {

    public static String stringToMd5Hash(String password) {
        final String saltSuffix = "e~w2Dd";
        final String saltPrefix = "P$73if";
        StringBuilder md5Hash = new StringBuilder(saltPrefix + password + saltSuffix);
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            for (int i = 0; i < 60000; i++) {
                messageDigest.update(md5Hash.toString().getBytes());
                byte[] digest = messageDigest.digest();
                md5Hash = new StringBuilder(saltPrefix + DatatypeConverter.printHexBinary(digest) + saltSuffix);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return md5Hash.toString().substring(saltPrefix.length(), md5Hash.length() - saltSuffix.length());
    }

}
