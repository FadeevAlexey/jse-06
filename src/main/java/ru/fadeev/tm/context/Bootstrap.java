package ru.fadeev.tm.context;

import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.command.application.ExitCommand;
import ru.fadeev.tm.command.application.HelpCommand;
import ru.fadeev.tm.command.project.*;
import ru.fadeev.tm.command.task.*;
import ru.fadeev.tm.command.user.*;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;
import ru.fadeev.tm.repository.ProjectRepository;
import ru.fadeev.tm.repository.TaskRepository;
import ru.fadeev.tm.repository.UserRepository;
import ru.fadeev.tm.service.ProjectService;
import ru.fadeev.tm.service.TaskService;
import ru.fadeev.tm.service.UserService;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.util.ConsoleHelper;

import java.util.*;

public final class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final TaskRepository taskRepository = new TaskRepository();

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository);

    private final TaskService taskService = new TaskService(taskRepository);

    private final UserService userService = new UserService(userRepository);

    private User currentUser = null;

    public void init() {
        registry(new ProjectCreateCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectEditCommand(this));
        registry(new ProjectTasksCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskEditCommand(this));
        registry(new TaskAddProjectCommand(this));
        registry(new UserLoginCommand(this));
        registry(new UserLogoutCommand(this));
        registry(new UserCreateCommand(this));
        registry(new UserEditCommand(this));
        registry(new UserUpdatePasswordCommand(this));
        registry(new UserProfileCommand(this));
        registry(new ExitCommand(this));
        registry(new HelpCommand(this));
        initUser();
        start();
    }

    private void initUser(){
        User user = new User("User");
        user.setHashPassword(HashUtil.stringToMd5Hash("user"));
        User admin = new User("Admin");
        admin.setHashPassword(HashUtil.stringToMd5Hash("admin"));
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            try {
                command = ConsoleHelper.readString();
                execute(command);
            } catch (RuntimeException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void registry(final AbstractCommand command) {
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null)
            throw new IllegalCommandNameException("Wrong command name");
        if (!abstractCommand.isPermission(currentUser))
            throw new AccessDeniedException("Access denied");
        abstractCommand.execute();
    }

    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public UserService getUserService() {
        return userService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

}