package ru.fadeev.tm.entity;

import ru.fadeev.tm.enumerated.Role;

import java.util.UUID;

public class User {

    String name;

    String hashPassword = "";

    Role role = Role.USER;

    String id = UUID.randomUUID().toString();

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getHashPassword() {
        return hashPassword;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setHashPassword(String hashPassword) {
        this.hashPassword = hashPassword;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "name = " + name + '\'' +
                ", role=" + role +
                ", id='" + id + '\'' +
                '}';
    }

}