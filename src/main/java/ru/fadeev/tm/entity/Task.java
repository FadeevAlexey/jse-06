package ru.fadeev.tm.entity;

import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.UUID;

public class Task implements IEntity {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private String projectId = "";

    private Date startDate;

    private Date finishDate;

    private String userId;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + ConsoleHelper.dateToString(startDate) +
                ", finishDate=" + ConsoleHelper.dateToString(finishDate) +
                '}';
    }

}