package ru.fadeev.tm.entity;

import ru.fadeev.tm.util.ConsoleHelper;

import java.util.Date;
import java.util.UUID;

public class Project implements IEntity {

    private String name = "";

    private String description = "";

    private String id = UUID.randomUUID().toString();

    private Date startDate;

    private Date finishDate;

    private String userId;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + ConsoleHelper.dateToString(startDate) +
                ", finishDate=" + ConsoleHelper.dateToString(finishDate) +
                '}';
    }

}